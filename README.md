Template Service Analysis
===
# Service Behaviour
The service has two responsibilities:
1. store templates requests passed from the user through the post endpoint;
2. send a message with the data to a rabbitmq channel;
3. retrieve the data from the mongo db after it was stored there.

The post endpoint does basic validation on the input before doing the send and save phase.
If the same content was stored 5 times into the database the code will return 42 as response to the user,
in any other case it will return the value that was stored back.
The get endpoint just retrieves data from the source of truth.

There is a part that is commented out which will read from the rabbit mq queue and store the data into the mongo
database.
This code requires to remove the send queue method from the post request to not duplicate data stored.

# Problem of the application
During runtime, we can find that the application has the following problems:
1. the application is missing bindings for rabbit mq on start up so spring cannot boot;
2. the application breaks the reactive paradigm in the post endpoint;
3. the get request is not working if two of the same elements are present in our database;

## Approach to the solution
The first action to boot up the code and debug issue was to create a docker compose file, so we can boot up the application and test it.

Our docker compose will contain a rabbitmq container and a mongo one, so we can store data and send it to the queue.

Having done the above the second part was to debug the missing bindings. 
In order to fix the spring dependencies I bumped the version of spring and added spring-cloud-stream-binder-rabbit as dependency.
At this point the application was able to startup.

To fix the endpoints we had to:
1. in the post endpoint we should use the reactive paradigm, so we chained the save with the response generation and removed the block() leaving send to queue as side effect still
2. in the get endpoint, if we had multiple entities of the same input, we will get an error because we would get more than one mono.
For this purpose we changed the method for the retrieval by the field to return just the first element using the next() on a flux so that the endpoint would return the same data but not 500. 

# Testing
To check that the code was behaving correctly I used the postman collection that was provided to verify each endpoint.
I tested:
1. storing and retrieving 1 template using the get and post endpoint;
2. I sent the wrong data to verify that the validation wasn't passing;
3. I saved 5 times to the database the same template to verify that we had 42 as response;
4. I tried to retrieve the data from the endpoint when there was more than one element with the same template field;
5. I checked that the messages were sent to the topic each time the endpoint was hit using the rabbitmq management ui;
6. I swapped the template manager config with the input so that I could verify that I could receive data;
7. I sent malformed data to verify that the data received is deserialized if and only if it's a valid template entity.
